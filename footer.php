<br>
<br>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="link">
                    <p>Botiga es liceu</p>
                    <a href="home">Inici</a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="link">
                <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'tema_menu_footer_interno'
                    ));
                ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="link">
                <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'tema_menu_footer_externo'
                    ));
                ?>
                </div>
            </div>
        </div>
    </div>
</footer>

</body>
<?php wp_footer(  )?>
</head>