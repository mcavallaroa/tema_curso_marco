<?php
    get_header();
    
?>

    

<div class="jumbotron jumbo-front">
    <h1>Bienvenidos a mi tienda</h1>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <img src="<?=get_theme_file_uri("inc/img/news.jpg")?>" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Últimas noticias</h5>
                    <a href="blog" class="btn btn-primary">Noticies</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img src="<?=get_theme_file_uri("inc/img/shop.jpg")?>" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Tienda online</h5>
                    <a href="tienda" class="btn btn-primary">Tienda</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img src="<?=get_theme_file_uri("inc/img/sale.jpg")?>" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Ofertas del día</h5>
                    <a href="ofertes" class="btn btn-primary">Ofertes</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    get_footer();
?>