<?php
require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';


function tema_curso_wp_config(){
   // echo "ejecutamos configuración";
   register_nav_menus(
       array(
           'tema_curso_wp'=>'Tema WP menú principal',
           'tema_menu_footer_interno'=>'Menú footer interno',
           'tema_menu_footer_externo'=>'Menú footer externo',
       )
    );

    add_theme_support( 'woocommerce', array(
        'thumbnail_image_width' => 255,
        'single_image_width'    => 255,
        'product_grid'          => array(
            'default_rows'  => 10,
            'min_rows'      => 5,
            'max_rows'      => 10,
            'default_columsn'   => 1,
            'min_columns'   => 1,
            'max_columns'   =>1,
        )
    ) );
    add_theme_support( 'wc-product-gallery-zoom');
    add_theme_support( 'wc-product-gallery-lightbox');
    add_theme_support( 'wc-prodcut-gallery-slider');

    if (!isset($content_width)){
        $content_width=600;
    }
}
add_action( "after_setup_theme", "tema_curso_wp_config", 0 );

function tema_curso_wp_scripts(){
    wp_enqueue_script( "bootstrap_js", get_theme_file_uri("inc/js/bootstrap.min.js"), array("jquery"),"4.5", true );
    wp_enqueue_style( "bootstrap_css", get_theme_file_uri("inc/css/bootstrap.min.css"),array(), "4.5","all" );
    wp_enqueue_style( "sytle_css", get_theme_file_uri("style.css"),array(),"1.0","all" );
}
add_action("wp_enqueue_scripts","tema_curso_wp_scripts",0 );